<?php

  class Calculator {
    static public $result;
    static function add($a, $b) : int {
      $result = $a + $b;
      // This does not work:
      // $this->result = $result;
      return $result;
    }
  }

  class Cal2 {
    static public $result;
    static function add($a, $b) : int {
      self::$result = $a + $b;
    }
  }

  Calculator::$result = Calculator::add(1, 2);
  echo Calculator::$result . "<br>";
  echo Calculator::add(2, 4) . "<br>";

  Cal2::add(1, 2);
  echo Cal2::$result . "<br>";

?>
