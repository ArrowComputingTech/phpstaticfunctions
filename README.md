These php programs demonstrate how to create and use static functions.


1.Used when you want to access class methods or properties without an instance.

2.Methods and Properties are marked as static so that it can be accessed with class names directly.

3. :: Scope resolution operator is used to access static methods and properties using only class name.
  i.e. - Car::applyBrake();
