<?php

  class StringHelper {
    static function countVowel($content) {
      $count = 0;
      $vowels = ['a','e','i','o','u'];
      $arr = str_split($content);
      foreach ($arr as $character) {
        if (in_array($character, $vowels)) {
          $count += 1;
        }
      }
      echo "There are " . $count . " vowels in " . $content . "<br>";
    }
  }

  $word = "Supernatural";
  StringHelper::countVowel($word);

?>
